// Base INSTALL ability module for kadi, used to perform initial installation of default kadi abilities
import path from 'path';
import fs from 'fs';
import { promises as fsPromises } from 'fs';
import chalk from 'chalk';
import AdmZip from 'adm-zip';
import axios from 'axios';

import {getKadiJSON, getAbilityJSON, getAbilitiesDir, getProjectJSON, getProjectJSONPath, saveAgentJSON, runExecCommand} from 'kadi-core';

const abilitiesDir          = getAbilitiesDir(); //path.join(rootDir, 'abilities');
const projectAgentPath      = getProjectJSONPath(); //path.join(rootDir, 'agent.json');

// Get the API URL from the agent.json
let kadijson = getKadiJSON();

// Define the API URL (Adjust according to your API)
const SEARCH_API_URL    = kadijson.api + '/search';
const GET_API_URL       = kadijson.api + "/get";
console.log(GET_API_URL);


//Updates the agent.json of kadi with the installed ability version
function updateBin(moduleName, version) {
    let abilityJSON = {};
    let projectAgentJSON = {};
    // Read the current agent.json's
    try {
        abilityJSON = getAbilityJSON(moduleName, version);
        projectAgentJSON = getProjectJSON();

    } catch (error) {
        console.error(`Error reading agent.json: ${error}`);
        return;
    }

    // Add all Ability bin commands to the kadi bin
    if(abilityJSON.bin){
          for (const command in abilityJSON.bin) {
            console.log(`Adding ${command} to kadi bin with path ${abilityJSON.bin[command]}`);
            projectAgentJSON.bin[command] = abilityJSON.bin[command];
          }
    }

    // Write the updated config back to config.json
    try {
        saveAgentJSON(projectAgentJSON, projectAgentPath);
        console.log(`agent.json updated for ${moduleName} to version ${version}.`);
    } catch (error) {
        console.error(`Error writing to agent.json: ${error}`);
    }
}

// Add ability to agent.json, only adds if not already listed
async function addAbilityToAgentJSON(name, version) {
    let data = {};

    // Read the current agent.json
    try {
        data = getProjectJSON(); //JSON.parse(content);
    } catch (error) {
        console.log(chalk.yellow('agent.json does not exist or is invalid. Creating a new one.'));
    }
    
    let abilityAlreadyListed = false;
    
    // Check if ability is already listed
    for (let ability of data.abilities) {
        if (ability.name === name) {
            abilityAlreadyListed = true;
            if(ability.version === version) {
                console.log(chalk.yellow(`${name}@${version} is already listed in agent.json.`));
                return;
            }
            ability.version = version;;
            
        }
    }
    
    if(!abilityAlreadyListed){
        // Add ability to agent.json
        data.abilities.push({'name': name, 'version': version});
    }

    // Save the updated agent.json
    saveAgentJSON(data, getProjectJSONPath());

    console.log(chalk.green(`agent.json has been updated with ${name}@${version}`));
}

// Ensure the abilities directory exists
async function ensureAbilitiesDirectory() {
    const dirPath = getAbilitiesDir(); //path.join(process.cwd(), 'abilities');
    try {
        await fsPromises.access(dirPath);
    } catch (error) {
        await fsPromises.mkdir(dirPath, { recursive: true });
        console.log(chalk.yellow('Created the abilities directory.'));
    }
}

//Installs the ability from the agent.json of kadi
async function installProjectAbilities(){

    //Get the abilities from the agent.json
    let projectAgentJSON = getProjectJSON()
    let projectAbilities = projectAgentJSON.abilities;


    //Install the abilities
    for (const ability of projectAbilities) {

        await installAbility(ability);

        //Update the agent.json bin with the installed ability
        updateBin(ability.name, ability.version);
        
    }
        
}

// Download and extract library
async function downloadAndExtractLibrary({ url, ability }) {

    console.log(chalk.green(`Downloading ${ability.name} v${ability.version}\n from: ${url} ...`));
    const response = await axios.get(url, { responseType: 'arraybuffer' });

    const zip = new AdmZip(response.data);
    const zipEntries = zip.getEntries(); // Get all entries (files and folders) in the ZIP

    let agentJsonPath = null; // This will hold the path of 'agent.json' within the ZIP

    // Find 'agent.json' in the ZIP
    zipEntries.forEach(entry => {
        if (entry.entryName.endsWith('agent.json')) {
            agentJsonPath = entry.entryName;
        }
    });

    if (!agentJsonPath) {
        console.log(chalk.red(`agent.json not found in ${ability.name} v${ability.version}.`));
        return;
    }

    const extractRoot = path.dirname(agentJsonPath); // Get the directory path of 'agent.json'
    const extractPath = path.join(abilitiesDir, ability.name, ability.version);
    fs.mkdirSync(extractPath, { recursive: true }); // Ensure the target directory exists

    // Extract everything from the directory of 'agent.json' downwards
    zipEntries.forEach(entry => {
        if (entry.entryName.startsWith(extractRoot)) {
            const relativePath = entry.entryName.substring(extractRoot.length); // Adjust the path
            const fullPath = path.join(extractPath, relativePath);

            if (entry.isDirectory) {
                fs.mkdirSync(fullPath, { recursive: true });
            } else {
                fs.writeFileSync(fullPath, entry.getData());
            }
        }
    });

    console.log(chalk.green(`${ability.name} version ${ability.version} has been extracted to ${extractPath}`));

}

//Recursive function to install abilities and their dependencies
async function installAbility(ability, updateJSON = false){
    await ensureAbilitiesDirectory();

    // let version = ability.version ? ability.version : 'latest';
    let abilityDirPath = path.join(abilitiesDir, ability.name , ability.version ? ability.version : 'latest');
    
    try {
        await fsPromises.access(abilityDirPath);
        console.log(chalk.yellow(`${ability.name} v${ability.version} is already installed.`));
        // Even if installed, you may still want to run the init command if dependencies might have changed
    } 
    catch (error) {
        // Directory does not exist, implying ability is not installed
        console.log(chalk.blue(`Installing ${ability.name} v${ability.version ? ability.version : ' latest'}...`));
        try {
            
            const endpoint = ability.version ? `${GET_API_URL}?ability=${ability.name}&version=${ability.version}` : `${GET_API_URL}?ability=${ability.name}`;
            const response = await axios.get(endpoint);
            if (response.status === 200) {

                const { name, lib, version, init, setup } = response.data;
                abilityDirPath = path.join(abilitiesDir, ability.name , version);
                console.log("Ability path: " + abilityDirPath);
                
                //When installing 'latest' version, check if the ability is already installed
                try{
                    await fsPromises.access(abilityDirPath)
                    console.log(chalk.yellow(`${ability.name} v${version} is already installed.`));
                    return;
                    
                }
                catch(error){
                    console.log("Downloading and extracting library");
                    if (lib) await downloadAndExtractLibrary({ url: lib, ability: {name: name, version: version}});

                    // Run a pre-install command if it exists
                    if (setup) {
                        console.log(chalk.green(`Running setup for ${ability.name} v${ability.version}...`));
                        await runExecCommand(name, version, setup);
                    }

                    

                    //Begin installing dependencies recursively
                    if (response.data.abilities) {
                        for (const currentAbility of response.data.abilities) {
                            await installAbility(currentAbility);
                        }
                    }
                    
                    // If root ability, update the agent.json (ability dependencies are not added to agent.json)
                    if(updateJSON){
                        await addAbilityToAgentJSON(name, version);
                    }

                    // Run init command for dependencies recursively first (assuming dependencies are included in response)
                    // After all dependencies are installed, run the init command if it exists
                    if (init) {
                        console.log(chalk.green(`Running init for ${ability.name} v${version}...`));
                        await runExecCommand(name, version, init);
                    }
                }
                
            } else {
                console.log(chalk.red('Ability not found.'));
            }
        } 
        catch (error) {
            console.error(chalk.red('An error occurred:'), error.message);
        }
    }   
    
}

//Entry point for module, installs abilities from agent.json
export default async function(args) {
    const [moduleName, version, ...flags] = args;

    //Perform install based upon abilities in agent.json from root directory
    if(moduleName === undefined) {
        console.log("Installing abilities from agent.json");
        installProjectAbilities();
    }
    else{
        console.log("Installing ability from command line");
        await installAbility({name: moduleName, version: version}, true);

    }
};
