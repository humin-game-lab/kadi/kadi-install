# kadi-install

## Description
-----------

KADI is a versatile command-line tool designed for managing agent abilities, simplifying dependency management, and automating agent setup and execution processes. It facilitates the initialization of agent projects, installation of abilities with dependency resolution, and execution of custom commands defined within agent configurations.  This repo holds the `install` ability of kadi

**install**
During install all abilities will be downloaded, unpacked and placed in the `abilities` folder (which will be created if needed).  Each ability downloaded will have all dependencies installed recursively.  Once a specific version of an ability is installed, it will not be downloaded or reinstalled again, even if later dependencies/abilities require it.


Commands
--------
### [`kadi install [abilityName] [version]`](https://gitlab.com/humin-game-lab/kadi/kadi-install)

Installs an ability. If an ability name is provided, it installs that specific ability. Without specifying an ability name, it installs all abilities listed in the `agent.json`'s abilites.

**Example:**

*   Install a specific ability, if no version is supplied it will install latest registered with server:
    
    `kadi install logging [version]`
    
*   Install all abilities from `agent.json`:
    
    `kadi install`
    

After installing abilities and their dependencies, the `init` command specified in the `agent.json` of each ability (if any) is executed to perform necessary setup tasks.

Roadmap Features
---------

* **Refactor**
    * **KADI INSTALL** 
        * ~~need to make sure when using `install ability` command that it checks to the `agent.json` file first to make sure its not already installed, before tyring to add it, as well as checking the abilities folder/version to see if it already exist (it should check both and add to both independently)~~
        * ~~Create pre and post installation commands (currently only post exist, init), update the agent.json to support~~
        * Provide capability for global (-g) installs, which get stored in the kadi `abilities` folder, or some global folder
            * should extend kadi abilities, where module adds items to kadi agent.json bin property, so new commands can be called using the following format: `kadi newAbilityFromGlobalModule`
            * on install, will make the following changes:
                * projectDirPath = kadi root directory
                * abilitiesDirPath = kadit abilities directory
        * ~~Update init commands to be run from the ability directory, and not the current working directory (pwd).~~
        * ~~Add ability to install latest, or use version~~
